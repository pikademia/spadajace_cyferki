﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Parzystosc : MonoBehaviour
{
    [SerializeField] Text txtWarunek;
    bool czyNastepnaCyfraMaBycParzysta;
    int kliknietaCyferka;
    bool czyKliknietaLiczbaJestParzysta;
    string wiadomoscOParzystosci;
    void Start()
    {
        LosujWarunek();
    }


    public void LosujWarunek()
    {
        if(GetComponent<GameManager>().PobierzIlePunktowZostalo() == 1)
        {
            czyNastepnaCyfraMaBycParzysta = false;
            wiadomoscOParzystosci = "Nieparzysta";
            txtWarunek.color = Color.yellow;
        }
        else
        {
            int losowaLiczba = Random.Range(0,2);
            if(losowaLiczba == 0)
            {
                czyNastepnaCyfraMaBycParzysta = true;
                wiadomoscOParzystosci = "Parzysta";
                txtWarunek.color = Color.white;
            }
            else
            {
                czyNastepnaCyfraMaBycParzysta = false;
                wiadomoscOParzystosci = "Nieparzysta";
                txtWarunek.color = Color.yellow;

            }
        }
        txtWarunek.text = wiadomoscOParzystosci;
    }

    public void PobierzKliknietaCyferke(int cyferka)
    {
        kliknietaCyferka = cyferka;
        SprawdzKlikniecie();
    }

    void SprawdzKlikniecie()
    {
        if(kliknietaCyferka % 2 == 0)
        {
            czyKliknietaLiczbaJestParzysta = true;
        }
        else
        {
            czyKliknietaLiczbaJestParzysta = false;
        }

        if(czyKliknietaLiczbaJestParzysta == czyNastepnaCyfraMaBycParzysta)
        {
            LosujWarunek();
        }
        else
        {
            GetComponent<GameManager>().GameOver();
        }

    }
}
