﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] GameObject btnCyferkaPrefab;
    [SerializeField] Transform kontenerNaCyferki;
    [SerializeField] Text txtLiczbaDocelowa;
    [SerializeField] Text txtLiczbaPunktow;
    [SerializeField] Text txtLicznikCzasu;
    Coroutine co;
    int liczbaDocelowa = 21;
    int liczbaPunktow = 0;
    float licznikPoczatkowy = 30f;
    float licznik;
    void Start()
    {
        co = StartCoroutine(TworzNowaCyferke());
        txtLiczbaDocelowa.text = liczbaDocelowa.ToString();
        licznik = licznikPoczatkowy;
    }

    public int PobierzIlePunktowZostalo()
    {
        return liczbaDocelowa - liczbaPunktow;
    }

    void ZmienKolorLicznika()
    {
        if(licznik <= 5)
        {
            txtLicznikCzasu.color = Color.red;
        }
        else if( licznik <= 10)
        {
            txtLicznikCzasu.color = Color.yellow;
        }
    }
    void OdliczajCzas()
    {
        if(licznik > 0)
        {
            licznik -= Time.deltaTime;
            txtLicznikCzasu.text = licznik.ToString("f2");
            ZmienKolorLicznika();
        }
    }
    void AktualizujElementyUI()
    {
        txtLiczbaPunktow.text = liczbaPunktow.ToString();
    }
    
    IEnumerator TworzNowaCyferke()
    {
        GameObject nowaCyferka = Instantiate(btnCyferkaPrefab, kontenerNaCyferki);
        int losowaPozycjaX = Random.Range(-140, 141);
        nowaCyferka.GetComponent<RectTransform>().anchoredPosition = new Vector2(losowaPozycjaX, 400f);
        yield return new WaitForSeconds(1f);
        co = StartCoroutine(TworzNowaCyferke());
    }

    private void Update()
    {
        OdliczajCzas();
        AktualizujElementyUI();
        SprawdzStatusGry();
    }

    public void DodajPunkty(int punkty)
    {
        liczbaPunktow += punkty;
    }

    public void SprawdzStatusGry()
    {
        if(licznik > 0)
        {
            if(liczbaPunktow == liczbaDocelowa)
            {
                WinTheGame();
            }
            else if(liczbaPunktow > liczbaDocelowa)
            {
                GameOver();
            }
        }
        else
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        SceneManager.LoadScene(1);
    }

    void WinTheGame()
    {
        SceneManager.LoadScene(3);
    }

    public void PrzejdzDoMenu()
    {
        SceneManager.LoadScene(0);
    }
}
